from nltk.tokenize import word_tokenize
import nltk
import xml.etree.ElementTree as et
from functools import reduce
import re


def clean_html_text(raw_html):
    pattern = re.compile('<.*?>')
    text = re.sub(pattern, '', raw_html)
    # return ''.join(text.split('\n'))
    return re.sub('[\s\-–=&:,!|#„”"\'`@\?;/\$%\\\(\)\{\}\[\]\.\+\*0-9]+', ' ', text).lower()


def clean_tags(raw_tags):
    tags_list = '>'.join(raw_tags.split('<')).split('>')
    return [tag for tag in tags_list if tag]


doc_root = et.parse('Posts.xml').getroot()

posts_set = []
counter = 0
for row in doc_root.findall('row'):
    raw_post = row.get('Body')
    raw_tags = row.get('Tags')
    if raw_tags:
        counter += 1
        post = clean_html_text(raw_post)
        for tag in clean_tags(raw_tags):
            posts_set.append((post, tag))
            print((post, tag))
    if counter == 153:
        break;


raw_train_set = posts_set[1:150]
raw_test_set = [post[0] for post in posts_set[151:153]]

all_train_words = set(word.lower() for passage in raw_train_set for word in word_tokenize(passage[0]))
train_set = [({word: (word in word_tokenize(x[0])) for word in all_train_words}, x[1]) for x in raw_train_set]

classifier = nltk.NaiveBayesClassifier.train(train_set)

for test_sentence in raw_test_set:

    print('SENTENCE:')
    print(test_sentence)
    test_sent_features = {word.lower(): (word in word_tokenize(test_sentence.lower())) for word in all_train_words}
    result = classifier.prob_classify(test_sent_features)

    prob_results = []
    for tag in result.samples():
        prob_results.append((tag, result.prob(tag)))

    values = [result[1] for result in prob_results]
    avg = reduce(lambda x, y: x + y, values) / len(values)

    print('TAG:')
    print(max(prob_results, key=lambda item: item[1])[0])
    print(sorted(prob_results, key=lambda item: item[1], reverse=True)[:3])
